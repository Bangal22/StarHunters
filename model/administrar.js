var fs = require('fs');

admin = [];

module.exports = {

    guardarDades:function(cordenades,taulell){
        let dadesCanva = {taulell:taulell, cordenadesEstrelles : cordenades};
        let path = './config/dadesTaulell.json';
        fs.writeFileSync(path,JSON.stringify(dadesCanva))
    },

    guardarAdmin:function(ad){
        if(admin.length > 0 ) return {accio : 'alert'};
        else {
            admin.push(ad);
            return {accio : 'succes'};
        }
    },  

    borrarDades:function(id){
        if(admin[0] == id) admin.shift();
    }

}