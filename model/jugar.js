let fs = require('fs')
let usuaris = [];

module.exports ={
    obtenirDades: function(){ 
        let path = './config/dadesTaulell.json';
        let dades = fs.readFileSync(path);
        return JSON.parse(dades);
    },

    nouJuagador:function(usuari){
        let path = './config/usuaris.json';
        if(typeof usuaris === 'string')usuaris = JSON.parse(usuaris);
        usuaris.push(usuari);
        
        fs.writeFileSync(path, JSON.stringify(usuaris));
    },

    obtenirDadesNau:function(){
        let path = './config/usuaris.json';
        let dades = fs.readFileSync(path);
        return JSON.parse(dades);
    },

    guardaDadesNau:function(naus){
        let path = './config/usuaris.json';
        usuaris = naus;
        fs.writeFileSync(path, JSON.stringify(naus));
    },

    borrarNau:function(id){
        let path = './config/usuaris.json';
        let dades = fs.readFileSync(path);
        let jugadors = JSON.parse(`${JSON.parse(JSON.stringify(dades.toString()))}`);
        for (const key in jugadors) {
            if(jugadors[key].id == id) {jugadors.splice(key,1);break;}
        }

        fs.writeFileSync(path, JSON.stringify(jugadors));
        return fs.readFileSync(path);
    },

    guardarDadesEstrelles:function(dadesTaulell){
        let path = './config/dadesTaulell.json';
        fs.writeFileSync(path, JSON.stringify(dadesTaulell));
    },

    sumarPuntuacio:function(id, punts){
        let path = './config/usuaris.json';
        let dades = fs.readFileSync(path);
        let jugadors = JSON.parse(`${JSON.parse(JSON.stringify(dades.toString()))}`);
        for (const key in jugadors) {
            if(jugadors[key].id == id) {
                jugadors[key].puntuacio = punts;
            }
        }

        fs.writeFileSync(path, JSON.stringify(jugadors));
    },

    actualitzarTemps:function(temps) {
        let path = './config/dadesTaulell.json';
        let dades = fs.readFileSync(path);
        let dadesTaulell = JSON.parse(`${JSON.parse(JSON.stringify(dades.toString()))}`);
        dadesTaulell.taulell.tempsRestant = temps;
        fs.writeFileSync(path, JSON.stringify(dadesTaulell));
    }

    
}