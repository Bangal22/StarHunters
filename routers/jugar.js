var router = require('express').Router();   

const jugarController = require('../controller/joc/jugar');

router.get('/jugar', jugarController.obtenirDades);

module.exports = router;