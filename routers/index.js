var router = require('express').Router();

const indexController = require('../controller/index');

router.get('/', indexController.index);
router.post('/', indexController.opcioJugarAdmin);

module.exports = router;