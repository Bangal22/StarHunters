var jugarModel = require('./model/jugar');
var modelAdministrar = require('./model/administrar');
var colorsJS = require('./public/javascript/transformacioColor');
var stop = false;
var puntuacioMax;
var timerId;
var intervals = [];

const express = require('express');
const app = express();
const server = require('http').createServer(app);
const WebSocket = require('ws');
const wss = new WebSocket.Server({server: server});
const body_parser = require('body-parser');

app.set('views', './view');
app.set('view engine', 'ejs');
app.use(body_parser.urlencoded({extended:true}));
app.use('/',require('./routers/index'));
app.use('/',require('./routers/jugar'));
app.use('/',require('./routers/nomJugador'));
app.use(express.static(__dirname));
app.use(express.json());

app.get('/nomJugador', (req, res) =>  {
	res.render('nomJugador',{});
});

app.post('/nomJugador', (req, res) => {
	let user = req.body.user;
	res.redirect('http://localhost:1000/jugar?nom='+user);
});	

app.get('/administrar', (req, res) =>  {
	res.render('administrar',{dades : false, coordenades: cordenades});
});

app.post('/administrar', (req, res) => {
    let cordenadesEstrelles = posicionsRandom(req);
	
    let play = req.body.play;
	puntuacioMax = req.body.puntuacioMax;
	let temps = req.body.temps;
	

    if (play === undefined) {
		stop = true;

    } else {
		stop = false;
        let taulell = {width : req.body.width, height : req.body.height, puntuacioMax : puntuacioMax, tempsRestant : temps, tempsTotal: temps};
        modelAdministrar.guardarDades(cordenadesEstrelles,taulell);
        res.render('administrar',{dades : true, cordenades : cordenadesEstrelles, cordenadesTaulell : taulell});

    }

});

wss.on('connection', function(ws,peticio){
	let id = peticio.connection.remotePort;
	ws.on('message', function (message){
		let params = message.toString();
		let object =  JSON.parse(params);
		gestio(ws,object, id);
	})
	ws.on('close', () => {
		console.log('Connexio tancada del jugador ' + ws._socket.remotePort)
		let naus = jugarModel.borrarNau(ws._socket.remotePort);
		jugarModel.guardaDadesNau(JSON.parse(naus.toString()));
		modelAdministrar.borrarDades(ws._socket.remotePort)
		jugadorDesconectat(JSON.stringify({accio : 'connexioTancada' , id : ws._socket.remotePort}))
	})
})

function gestio (ws,missatge,id){
	if (stop === false) {
		switch(missatge.accio){
			case "nouJugador" : nouJugador(ws,id, missatge.user); temps(id) ;break ;
			case "nouAdmin" : nouAdmin(ws,id) ; break;
			case "moure" : moure(ws,id); break ;
			case "cordenades" : cordenades(missatge.cord,id); break ;
			case "eliminarEstrella" : 
				eliminarEstrella(ws, missatge.id, missatge.punts, missatge.idJugador); 
				estrellesAconseguides(ws,missatge.idJugador,missatge.punts, missatge.nom); 
				break;
			default : '' ; break ;
		}

	} else {
		intervals.forEach(element => {clearInterval(element.interval)});
		broadcast(JSON.stringify({accio:"stop"}));
	}
	
}

function broadcast(missatge) {
	wss.clients.forEach(function each(client) {
		client.send(missatge);
	});
}


function nouJugador(ws,id, user){
	let dadesTaulell = jugarModel.obtenirDades();
	let tempsRestant = dadesTaulell.taulell.tempsRestant;
	let tempsMax = dadesTaulell.taulell.tempsTotal;

	let x = Math.random() * dadesTaulell.taulell.width;
    let y = Math.random() * dadesTaulell.taulell.height;
	if(x > 50){if( x+40 >  dadesTaulell.taulell.width) x = dadesTaulell.taulell.width-40;}
	if(y > 50){if( y+40 > dadesTaulell.taulell.height) y = dadesTaulell.taulell.height-40;}

	jugarModel.nouJuagador({
		id: id, 
		x : Math.random() * dadesTaulell.taulell.width , 
		y : Math.random() *dadesTaulell.taulell.height,
		r : 0,
		color : colorRandom() ,
		puntuacio : 0,
		nom: user
	});

	let naus = jugarModel.obtenirDadesNau();
	ws.send(JSON.stringify({ accio: 'inicialitzarJugador' , idJugador: id}));
	broadcast(JSON.stringify({ accio: 'enviarNaus' , naus : naus, temps: tempsRestant, tempsMax: tempsMax}));
}

function nouAdmin (ws,id){
	let result = modelAdministrar.guardarAdmin(id);
	ws.send(JSON.stringify(result));
}


function moure (ws,id){
	let naus = jugarModel.obtenirDadesNau();
	let puntuacio;
	let nom;

	for (const key in naus) {
		if(naus[key].id === id) {
			puntuacio = naus[key].puntuacio;
			nom = naus[key].nom;
			break;
		} 
	}

	ws.send(JSON.stringify({accio : 'idJugador' , id : id, puntuacio: puntuacio, nom: nom}));
}


function cordenades(cordenades,id){
	let naus = jugarModel.obtenirDadesNau();

	for (const key in naus) {
		if(naus[key].id === id) naus[key] = {id :id, x : cordenades.x, y : cordenades.y, r : cordenades.r, color : cordenades.color, puntuacio : cordenades.puntuacio, nom: cordenades.nom}
	}

	jugarModel.guardaDadesNau(naus);
	broadcast(JSON.stringify({accio : 'canviPosicio' , nau : {id :id, x : cordenades.x, y : cordenades.y, r : cordenades.r, color : cordenades.color, puntuacio: cordenades.puntuacio, nom: cordenades.nom}}))
}


function eliminarEstrella(ws, id, punts, idJugador) {
	let dadesTaulell = jugarModel.obtenirDades();

	for (const key in dadesTaulell.cordenadesEstrelles) {
		if(id == dadesTaulell.cordenadesEstrelles[key].id){dadesTaulell.cordenadesEstrelles.splice(key, 1);break;}
	}

	jugarModel.guardarDadesEstrelles(dadesTaulell);
	broadcast(JSON.stringify({accio : "eliminarEstrella", id : id}));

}

function estrellesAconseguides(ws,idJugador,punts, nom){
	let puntuacio = parseInt(punts)+1;

	if (puntuacio == parseInt(puntuacioMax)) {

		broadcast(JSON.stringify({accio:"jocFinalitzat", guanyador: nom}));

		intervals.forEach(element => {clearInterval(element.interval)});

	} else {
		jugarModel.sumarPuntuacio(idJugador,puntuacio);
		ws.send(JSON.stringify({accio : "actualitzarPunts", punts : puntuacio, idJugador: idJugador}));
	}
	
}


function colorRandom()  {
    let color = "#" + Math.floor(Math.random()*16777215).toString(16).padStart(6, '0').toUpperCase();
	let filter = colorsJS.hexToRgb(color);
	const rgb = new colorsJS.Color(filter[0],filter[1],filter[2])
	const solver = new colorsJS.Solver(rgb);
	const result = solver.solve();
	return result.filter;
}


function jugadorDesconectat (json){
	broadcast(json);
}


function posicionsRandom (info){
    let cordenadesEstrelles = [];
    for (let index = 0; index < info.body.estrelles; index++) {
        let x = Math.round(Math.random() * info.body.width);
        let y = Math.round(Math.random() * info.body.height);
		
		if(x > 50){if( x+40 > info.body.width) x = info.body.width-40;}
		if(y > 50){if( y+40 > info.body.height) y = info.body.height-40;}
	
        cordenadesEstrelles.push({id:index+1, x:x, y:y});
    }
    return cordenadesEstrelles;  
}

function finalitzarJocTemps(id) {
	let nausJson = jugarModel.obtenirDadesNau();
	let nomGuanyador;
	let puntuacio = 0;

	for (const key in nausJson) {

		if (parseInt(nausJson[key].puntuacio) > puntuacio) {
			nomGuanyador = nausJson[key].nom;
			puntuacio = parseInt(nausJson[key].puntuacio);
		}
	}

	broadcast(JSON.stringify({accio:"jocFinalitzat", guanyador: nomGuanyador, id: id}));
}

function temps(id) {
	let dadesTaulell = jugarModel.obtenirDades();
	let seconds = dadesTaulell.taulell.tempsRestant;
	let max = dadesTaulell.taulell.tempsTotal;

	timerId = setInterval(() => { 
        seconds--;
		console.log(seconds);

        if(seconds <= 0) {
			finalitzarJocTemps(id);

			intervals.forEach(element => {clearInterval(element.interval)});

        } else {
			jugarModel.actualitzarTemps(seconds);
			broadcast(JSON.stringify({accio: "rellotge", temps: seconds, max: max}));
		}

    }, 1000);

	intervals.push({interval: timerId});
}

server.listen(1000, () => console.log('listening on port 1000'))

