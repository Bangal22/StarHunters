document.addEventListener('DOMContentLoaded',init);
let keys = {dreta : false, esquerra : false, amunt : false, abaix : false};


var connexio;
var jugadorId;

function init (){
    document.addEventListener('keydown', keyDown);
    document.addEventListener('keyup', keyUp);
    
    connexio = new WebSocket('ws://localhost:1000');

    connexio.onopen = function(peticio) {
        var queryString = window.location.search;
        var urlParams = new URLSearchParams(queryString);
        var nom = urlParams.get('nom');
		connexio.send(JSON.stringify({accio:'nouJugador', user: nom}));	
	};
    connexio.onmessage = function(msg) {
        let json = JSON.parse(msg.data);
        switch(json.accio){
            case 'idJugador' : moureNau(json.id, json.puntuacio, json.nom) ; break ;
            case 'inicialitzarJugador': jugadorId = json.idJugador; break;
            case 'enviarNaus' : posarNaus(json) ; break ;
            case 'canviPosicio' : guardarCord(json); amagarEstrella(json); break ;
            case 'connexioTancada' : eliminarJugador(json); break ;
            case 'eliminarEstrella' : eliminarEstrella(json); break ;
            case 'actualitzarPunts' : if(json.idJugador == jugadorId) actualitzarPunts(json.punts); break;
            case 'rellotge': rellotge(json.temps, json.max); break;
            case 'stop': alert("L'administrador ha finalizat el joc!"); window.location.href = "http://localhost:1000/"; break;
            case 'jocFinalitzat': finalitzatJoc(json); break;
        }
    }
}

function eliminarEstrella(missatge){
    let estrella = document.getElementById(missatge.id);
    if(estrella!=null) estrella.remove(); 
}
function posarNaus (missatge){
    document.getElementById('temps').setAttribute("title", missatge.tempsMax.toString());
    document.getElementsByClassName('seconds')[0].textContent = missatge.temps.toString();
    missatge.naus.forEach(element => {
        let existeix = document.getElementById(element.id);
        if(existeix == undefined){
            let nau ='<g class="contenidor-nau"><image width="40" height="40" id="'+element.id+'" xlink:href="../public/img/nau.png" x="'+element.x+'" y="'+element.y+'" style="'+element.color+'"/></g>'
            let dibu = document.getElementById('dibu');
            dibu.innerHTML += nau;
        }
    });
}
function moureNau (params, puntuacio, nom){
    let nau = document.getElementById(params);
    let taulell = document.getElementById('dibu');
    
    let taulellY = taulell.getAttribute('height');
    let taulellX = taulell.getAttribute('width');
    let color = nau.getAttribute('style');

    let x = nau.getAttribute('x');
    let y = nau.getAttribute('y');

    if (keys.esquerra)  x = ('x',nau.getAttribute('x')-10);
    if (keys.dreta)     x = ('x',nau.getAttribute('x')-0+10);
    if (keys.amunt)     y = ('y',nau.getAttribute('y')-10);
    if (keys.abaix)     y = ('y',nau.getAttribute('y')-0+10);

    if(x < 0 ) x = 0;
    if(y < 0) y = 0;
    if(x > 40){
        if( x-0+40 > taulellX) x = taulellX-40;
    }
    if(y > 40){
        if( y-0+40 > taulellY) y = taulellY-40;
    }

    let cordenades = JSON.stringify({accio: 'cordenades', cord : { x : x, y : y, r : 0, color : color, puntuacio: puntuacio, nom: nom}});
    
    connexio.send(cordenades);

    nau.setAttribute('x', x);
    nau.setAttribute('y', y);
}
function guardarCord(param){
    let nau = document.getElementById(param.nau.id);
    nau.setAttribute('x', param.nau.x);
    nau.setAttribute('y', param.nau.y);
}
function eliminarJugador(param){
    document.getElementById(param.id).remove();
}
function keyUp (event){
    let caracter = event.key;
    switch (caracter) {
        case 'ArrowLeft':   case 'a' : keys.esquerra = false; break;
        case 'ArrowRight' : case 'd' : keys.dreta = false; break;
        case 'ArrowUp':     case 'w' : keys.amunt = false; break;
        case 'ArrowDown':   case 's' : keys.abaix = false; break;
    }
    let cordenades = JSON.stringify({accio : 'moure'});
    connexio.send(cordenades);
}
function keyDown (event){
    let caracter = event.key;
    switch (caracter) {
        case 'ArrowLeft':   case 'a' : keys.esquerra = true; break;
        case 'ArrowRight' : case 'd' : keys.dreta = true; break;
        case 'ArrowUp':     case 'w' : keys.amunt = true; break;
        case 'ArrowDown':   case 's' : keys.abaix = true; break;
    }
    let cordenades = JSON.stringify({accio : 'moure'});
    connexio.send(cordenades);
}

function amagarEstrella (naus) {
    let estrelles = document.querySelectorAll('.estrelles');

    for (let index = 0; index < estrelles.length; index++) {

        let x = estrelles[index].getAttribute('x');
        let y = estrelles[index].getAttribute('y');
        let totalX = Math.abs(naus.nau.x - x);
        let totalY = Math.abs(naus.nau.y - y);
        
        if ( totalX <=  10 && totalY <=  20) {
			let estrella = estrelles[index].id;
            let puntuacio = parseInt(document.getElementById('puntuacio').value);
            connexio.send(JSON.stringify({accio : 'eliminarEstrella',id : estrella, idJugador : naus.nau.id, punts : puntuacio, nom: naus.nau.nom }));
            break;
		}
    }
}

function actualitzarPunts(punts){
    document.getElementById('puntuacio').value = punts;
}

function finalitzatJoc (json) {
    alert("El guanyador és: "+json.guanyador+"!"); 

    setTimeout( () => {window.location.href = "http://localhost:1000/"}, 2000);
}


function rellotge(secondsServidor, maxServidor) {
    let progressElm = document.getElementsByClassName('progress')[0];
    let circumference = 2 * Math.PI * progressElm.getAttribute('r');

    progressElm.style.strokeDasharray = circumference;
    progressElm.style.strokeDashoffset = circumference * 0;

    let seconds = secondsServidor;
    
    let max = maxServidor;

    let secondsElm = document.getElementsByClassName('seconds')[0];

    let percentage = seconds/max * 100;
    
    progressElm.style.strokeDashoffset = circumference - (percentage/100) * circumference;

    secondsElm.textContent = seconds.toString().padStart(2, '0');
}