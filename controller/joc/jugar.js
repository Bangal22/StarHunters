var jugarModel = require('../../model/jugar');

module.exports={
    obtenirDades:function(req,res){
        let dades = jugarModel.obtenirDades(); 
        let dadesNau = jugarModel.obtenirDadesNau();   
        res.render('jugar',{dades : dades, dadesNau : dadesNau})
        
    },
}