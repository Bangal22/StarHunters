document.addEventListener('DOMContentLoaded', init)

function init() {
    var connexio;
    connexio = new WebSocket('ws://localhost:1000');
    connexio.onopen = function(peticio) {
        connexio.send(JSON.stringify({accio:'nouAdmin'}));	
    };
    connexio.onmessage = function(msg){
        let missatge = JSON.parse(msg.data);
        if(missatge.accio == "alert"){
            alert("Ja hi ha un administrador connectat!");
            window.location.href = "http://localhost:1000/";
        }
    }

    
    let svg = document.getElementById('dibu');

    let width = document.getElementById('width');
    width.setAttribute('value',svg.getAttribute('width'));


    let height = document.getElementById('height');
    height.setAttribute('value',svg.getAttribute('height'));

    width.addEventListener('input', (event)=> svg.setAttribute('width',event.target.value));
    height.addEventListener('input', (event)=> svg.setAttribute('height',event.target.value));

}

